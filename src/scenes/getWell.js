module.exports = {
    name: 'getWell',
    type: 'menu',
    data: [
        {
            text: 'No hay estilos en un hospital,',
            next: 'getWell',
            size: '50px',
            color: '#000'
        },
        {
            text: 'solo vas a poder usar una bata',
            next: 'getWell',
            size: '50px',
            color: '#000'
        },
        {
            text: '¡Que te mejores!',
            next: 'getWell',
            size: '56px',
            color: '#000'
        }
    ],
    extras: 'GetWell',
    dataStyles: {
        marginBottom: '15%'
    }
};
