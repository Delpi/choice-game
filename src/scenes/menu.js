module.exports = {
    name: 'main',
    type: 'menu',
    data: [
        {
            text: 'DISCLAIMER',
            next: 'disclaimer'
        },
        {
            text: 'Bienvenid@ a',
            next: 'main',
            size: '56px',
            color: '#000'
        },
        {
            text: '¿QUE ESTILO Y2K VA CON VOS?',
            next: 'mainBranch',
            size: '72px',
            color: '#000'
        }
    ],
    extras: 'Menu',
    dataStyles: {
        marginBottom: '10%',
    }
};
