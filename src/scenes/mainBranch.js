module.exports = {
    name: 'mainBranch',
    type: 'choice',
    data: [
        {
            title: '¿Quién va a ser tu asesora de moda?',
            next: 'mainBranch'
        },
        {
            text: ' - ANA',
            next: 'anaOutfit'
        },
        {
            text: ' - MIA',
            next: 'miaOutfit'
        }
    ],
    extras: 'Menu'
};
