module.exports = {
    name: 'miaHelp',
    type: 'choice',
    data: [
        {
            text: 'MIA te va a ayudar.',
            next: 'miaHelp'
        },
        {
            text: '¿Cómo vas a adelgazar?',
            next: 'miaHelp'
        },
        {
            text: ' - Contando calorías',
            next: 'miaNotEnough'
        },
        {
            text: ' - Dejando de comer',
            next: 'miaNotEnough'
        }
    ],
    extras: 'MiaHelp'
};

