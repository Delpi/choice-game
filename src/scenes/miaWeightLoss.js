module.exports = {
    name: 'miaWeightLoss',
    type: 'choice',
    data: [
        {
            text: 'MIA dice que TENÉS que perder peso porque todo te queda feo',
            next: 'anaWeightLoss'
        },
        {
            text: '¿Qué hacés?',
            next: 'miaWeightLoss'
        },
        {
            text: ' - Perder Peso',
            next: 'miaNotEnough'
        },
        {
            text: ' - No escuchar a MIA',
            next: 'miaPressure'
        }
    ],
    extras: 'MiaHelp'
};

