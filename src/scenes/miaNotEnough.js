module.exports = {
    name: 'miaNotEnough',
    type: 'choice',
    data: [
        {
            text: 'MIA dice:',
            next: 'miaNotEnough'
        },
        {
            text: '¡Muy Bien! Pero no es suficiente y hoy ya comiste mucho.',
            next: 'miaNotEnough'
        },
        {
            text: '¿Qué le decís?',
            next: 'miaNotEnough'
        },
        {
            text: ' - ¡Voy a Vomitar!',
            next: 'miaPressure'
        },
        {
            text: ' - ¡Dejame en paz!',
            next: 'miaPressure'
        }
    ],
    extras: 'MiaNotEnough'
};
