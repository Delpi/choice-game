module.exports = {
    name: 'miaCorrect',
    type: 'choice',
    data: [
        {
            text: 'MIA dice:',
            next: 'miaCorrect'
        },
        {
            text: '¡Buena Elección!\nPero no te queda lindo...',
            next: 'miaCorrect'
        },
        {
            text: '¿Qué hacés?',
            next: 'miaCorrect'
        },
        {
            text: ' - Perder Peso',
            next: 'miaHelp'
        },
        {
            text: ' - Hablar con MIA al respecto',
            next: 'miaWeightLoss'
        }
    ],
    extras: 'MiaCorrect'
};

