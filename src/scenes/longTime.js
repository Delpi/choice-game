module.exports = {
    name: 'longTime',
    type: 'menu',
    data: [
        {
            text: 'Vas a estar acá un tiempo...',
            next: 'getWell',
            size: '56px',
            color: '#000'
        },
        {
            text: 'TU ESTILO ES',
            next: 'getWell',
            size: '66px',
            color: '#000'
        }
    ],
    extras: 'LongTime',
};
