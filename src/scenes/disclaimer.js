module.exports = {
    name: 'disclaimer',
    type: 'choice',
    data: [
        {
            text: 'Este juego es una simulación de TCAS y no es recomendado para personas que pasen o estén pasando por uno. Los trastornos alimenticios son graves y pueden ser fatales. Es importante buscar tratamiento temprano',
            next: 'disclaimer'
        },
        {
            text: 'ENTIENDO',
            next: 'main'
        }
    ],
    extras: 'Menu',
    dataStyles: {
        marginBottom: '10%'
    }
};
