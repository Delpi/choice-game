module.exports = {
    name: 'miaOutfit',
    type: 'choice',
    data: [
        {
            title: '¿Cuál es tu outfit favorito?',
            next: 'miaOutfit'
        },
        {
            text: ' - Crop top y falda tiro bajo',
            next: 'miaCorrect'
        },
        {
            text: ' - Crop top y falda tiro alto',
            next: 'miaWrong'
        }
    ],
    extras: 'MiaOutfit'
};

