module.exports = {
    name: 'miaUnderweight',
    type: 'choice',
    data: [
        {
            text: 'Pasaste mucho tiempo con MIA y ahora estás',
            next: 'miaUnderweight'
        },
        {
            text: 'por debajo de tu peso sano',
            next: 'miaUnderweight'
        },

        {
            text: '¿Qué hacés?',
            next: 'miaUnderweight'
        },
        {
            text: ' - Ir al médico',
            next: 'longTime'
        },
        {
            text: ' - Seguir hablando con MIA',
            next: 'longTime'
        }
    ],
    extras: 'MiaUnderweight'
};

