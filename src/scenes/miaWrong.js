module.exports = {
    name: 'miaWrong',
    type: 'choice',
    data: [
        {
            text: 'MIA dice:',
            next: 'mainBranch'
        },
        {
            text: 'Opción INCORRECTA',
            next: 'mainBranch'
        },
        {
            title: 'INTENTA DE NUEVO',
            next: 'mainBranch'
        }
    ],
    extras: 'MiaWrong'
};

