module.exports = {
    name: 'miaCongratulations',
    type: 'menu',
    data: [
        {
            text: '¡FELICITACIONES!',
            next: 'youWin',
            size: '56px',
            color: '#000'
        },
        {
            text: 'MIA te dejó en paz',
            next: 'youWin',
            size: '50px',
            color: '#000'
        },
        {
            text: 'GANASTE',
            next: 'youWin',
            size: '60px'
        },
        {
            text: 'TU ESTILO ES...',
            next: 'youWin',
            size: '56px',
            color: '#000'
        }
    ],
    extras: 'Congratulations'
};
