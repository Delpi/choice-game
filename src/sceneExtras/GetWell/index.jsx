import SceneImg from '../../assets/hospital2.png';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <img src={SceneImg}
                 alt='A hospital scene. I love the smell of disinfectancts in the morning, they smell like... pandemic'
                 className='imageBottomCenterSmall'
            />
        </div>
    );
}
