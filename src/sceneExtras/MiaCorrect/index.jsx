import Mia from '../auxiliary/Mia';
import SceneImg from '../../assets/diet_11.gif';
import './styles.css';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Mia />
            <img src={SceneImg}
                 alt='A person running away from the calories'
                 className='imageTopLeft'
            />
        </div>
    );
}
