import AnaImg from '../../../assets/PARA FONDO ANA.gif';
import './styles.css';

export default function Ana (props) {
    return (
        <div className='CharFrame AnaFrame'>
            <p className='anaText'>
                hi i'm ANA
            </p>

            <img src={AnaImg}
                 alt='A character named Ana'
            />
        </div>
    );
}
