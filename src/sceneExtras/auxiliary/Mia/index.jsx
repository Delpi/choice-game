import MiaImg from '../../../assets/PARA FONDO MIA.gif';
import './styles.css';

export default function Mia (props) {
    return (
        <div className='CharFrame MiaFrame'>
            <p className='miaText'>
                hi i'm MIA
            </p>

            <img src={MiaImg}
                 alt='A character named Mia'
            />
        </div>
    );
}
