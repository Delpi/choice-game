import './styles.css';
import ana from '../../assets/PARA FONDO ANA.gif';
import mia from '../../assets/PARA FONDO MIA.gif';

export default function Extra (props) {
    return (
        <div className='MenuCharacters'>
            <div className='CharFrame'>
                <p className='anaText'>
                    hi i'm ANA
                </p>

                <img src={ana}
                    alt='A character, named Ana'
                />
            </div>
            <div className='CharFrame'>
                <p className='miaText'>
                    hi i'm MIA
                </p>

                <img src={mia}
                     alt='A character, named Mia'
                />
            </div>
        </div>
    );
}
