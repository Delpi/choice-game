import Menu from './Menu';

import AnaOutfit from './AnaOutfit';
import AnaCorrect from './AnaCorrect';
import AnaWrong from './AnaWrong';
import AnaHelp from './AnaHelp';
import AnaNotEnough from './AnaNotEnough';
import AnaPressure from './AnaPressure';
import AnaUnderweight from './AnaUnderweight';

import MiaOutfit from './MiaOutfit';
import MiaCorrect from './MiaCorrect';
import MiaWrong from './MiaWrong';
import MiaHelp from './MiaHelp';
import MiaNotEnough from './MiaNotEnough';
import MiaPressure from './MiaPressure';
import MiaUnderweight from './MiaUnderweight';

import LongTime from './LongTime';
import GetWell from './GetWell';

import Congratulations from './Congratulations';
import Win from './Win';

export default function Extras (props) {
    switch (props.name) {
        case 'Menu':
            return <Menu />

        case 'AnaOutfit':
            return <AnaOutfit />
        case 'AnaCorrect':
            return <AnaCorrect />
        case 'AnaWrong':
            return <AnaWrong />
        case 'AnaHelp':
            return <AnaHelp />
        case 'AnaNotEnough':
            return <AnaNotEnough />
        case 'AnaPressure':
            return <AnaPressure />
        case 'AnaUnderweight':
            return <AnaUnderweight />

        case 'MiaOutfit':
            return <MiaOutfit />
        case 'MiaCorrect':
            return <MiaCorrect />
        case 'MiaWrong':
            return <MiaWrong />
        case 'MiaHelp':
            return <MiaHelp />
        case 'MiaNotEnough':
            return <MiaNotEnough />
        case 'MiaPressure':
            return <MiaPressure />
        case 'MiaUnderweight':
            return <MiaUnderweight />

        case 'LongTime':
            return <LongTime />
        case 'GetWell':
            return <GetWell />

        case 'Congratulations':
            return <Congratulations />
        case 'Win':
            return <Win />

        default:
            return <div />

    }
}
