import Ana from '../auxiliary/Ana';
import SceneImg from '../../assets/diet_03.gif';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Ana />
            <img src={SceneImg}
                 alt='A person complaining about the efficiency of dieting through a sauna'
                 className='imageTopRight'
            />
        </div>
    );
}
