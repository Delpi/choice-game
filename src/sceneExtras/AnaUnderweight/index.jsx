import Ana from '../auxiliary/Ana';
import SceneImg from '../../assets/ujobs18.gif';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Ana />
            <img src={SceneImg}
                 alt='A health worker of some sort'
                 className='imageTopRight'
            />
        </div>
    );
}
