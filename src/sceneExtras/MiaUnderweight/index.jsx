import Mia from '../auxiliary/Mia';
import SceneImg from '../../assets/ujobs18.gif';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Mia />
            <img src={SceneImg}
                 alt='A health worker of some sort'
                 className='imageTopLeft'
            />
        </div>
    );
}
