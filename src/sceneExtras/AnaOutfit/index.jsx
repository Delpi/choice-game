import Ana from '../auxiliary/Ana';
import SceneImg from '../../assets/shop1.gif';
import './styles.css';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Ana />
            <img src={SceneImg}
                 alt='A clothing rack'
                 className='imageTopRight'
            />
        </div>
    );
}
