import Ana from '../auxiliary/Ana';
import SceneImg from '../../assets/diet_14.gif';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Ana />
            <img src={SceneImg}
                 alt='A person losing it, presumably their weight'
                 className='imageTopRight'
            />
        </div>
    );
}
