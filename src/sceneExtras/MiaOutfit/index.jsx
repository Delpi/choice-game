import Mia from '../auxiliary/Mia';
import SceneImg from '../../assets/shop1.gif';
import './styles.css';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Mia />
            <img src={SceneImg}
                 alt='A clothing rack'
                 className='imageTopLeft'
            />
        </div>
    );
}
