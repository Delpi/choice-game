import Mia from '../auxiliary/Mia';
import SceneImg from '../../assets/diet_03.gif';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Mia />
            <img src={SceneImg}
                 alt='A person complaining about the efficiency of dieting through a sauna'
                 className='imageTopLeft'
            />
        </div>
    );
}
