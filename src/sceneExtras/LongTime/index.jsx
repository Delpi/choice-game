import SceneImg from '../../assets/hospital.png';
import './styles.css'

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <img src={SceneImg}
                 alt='A hospital bed'
                 className='imageBottomCenterSmall'
            />
        </div>
    );
}
