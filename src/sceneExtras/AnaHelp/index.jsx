import Ana from '../auxiliary/Ana';
import SceneImg from '../../assets/diet_05.gif';
import './styles.css';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Ana />
            <img src={SceneImg}
                 alt='A person explaining the difficulty of dieting'
                 className='imageCenterRight'
            />
        </div>
    );
}
