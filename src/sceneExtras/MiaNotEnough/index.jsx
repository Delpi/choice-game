import Mia from '../auxiliary/Mia';
import SceneImg from '../../assets/diet_14.gif';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Mia />
            <img src={SceneImg}
                 alt='A person losing it, presumably their weight'
                 className='imageTopLeft'
            />
        </div>
    );
}
