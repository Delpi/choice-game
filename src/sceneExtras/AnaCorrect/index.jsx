import Ana from '../auxiliary/Ana';
import SceneImg from '../../assets/diet_11.gif';
import './styles.css';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <Ana />
            <img src={SceneImg}
                 alt='A person running away from the calories'
                 className='imageTopRight'
            />
        </div>
    );
}
