import SceneImg from '../../assets/PNG CUP.png';
import './styles.css';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <img src={SceneImg}
                 alt='Another trophy. You won already.'
                 className='imageBottomCenter'
            />
        </div>
    );
}
