import SceneImg from '../../assets/PNG CUP 2.png';

export default function Extra (props) {
    return (
        <div className='gameSceneExtra'>
            <img src={SceneImg}
                 alt='A trophy. A Cruel Angel Thesis may be playing as well'
                 className='imageBottomRight'
            />
        </div>
    );
}
