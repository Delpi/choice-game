import Extras from '../../../sceneExtras';

import './styles.css';

function ChoiceItem (props) {
    const goNext = () => {
        if (props.onTransition) {
            props.onTransition(props.next ?? 'main');
        }
    };

    return (
        <div onClick={goNext}>
            <h1 className='sceneH'>
                {props.title}
            </h1>
            <p className='sceneP'>
                {props.text}
            </p>
        </div>
    );
}

export default function ChoiceScene (props) {
    const goNext = (name) => {
        if (props.onTransition) {
            props.onTransition(name ?? 'main');
        }
    };

    return (
        <div className='ChoiceScene'>
            {
                props.data.map((e, i) => (
                    <ChoiceItem
                        key={i}
                        onTransition={goNext}
                        {...e}
                    />
                ))
            }

            <Extras name={props.Extras} />
        </div>
    );
}
