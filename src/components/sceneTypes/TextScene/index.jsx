import Extras from '../../../sceneExtras';

export default function TextScene (props) {
    const goNext = () => {
        if (props.onTransition) {
            props.onTransition(props.data?.next ?? 'main');
        }
    }

    return (
        <div onClick={goNext}>
            <h1>
                {props.data?.text}
            </h1>

            <Extras name={props.Extras} />
        </div>
    );
}
