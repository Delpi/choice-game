import React from 'react';

import ChoiceScene from '../sceneTypes/ChoiceScene';
import TextScene from '../sceneTypes/TextScene';
import MenuScene from '../sceneTypes/MenuScene';

import scenes from '../../scenes';

function Scene (props) {
    switch (props.type) {
        case 'text':
            return <TextScene {...props} />
        case 'choice':
            return <ChoiceScene {...props} />
        case 'menu':
            return <MenuScene {...props} />
        default:
            return <p>Error: invalid scene type</p>
    }
};

export default function SceneViewer (props) {
    const findSceneByName = (name) => scenes.find(
        scene => scene.name.toLowerCase() === name.toLowerCase()
    );

    const [currentScene, setCurrentScene] = React.useState(
        findSceneByName('main')
    );

    const changeScene = (name) => setCurrentScene(
        findSceneByName(name)
    );

    return (
        <Scene
            onTransition={changeScene}
            {...currentScene}
        />
    );
}
