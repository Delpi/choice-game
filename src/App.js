import SceneViewer from './components/SceneViewer';

import './App.css';

function App() {
    return (
        <div className="App">
            <SceneViewer />
        </div>
    );
}

export default App;
